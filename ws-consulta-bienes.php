<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=utf-8');

if (isset($_POST['cod_bienes'])) {

    $servicio = "http://192.168.104.104/wsbienes/Service.asmx?WSDL"; //url del servicio

    $parametros = array(
        'cod_bienes' => filter_input(INPUT_POST, 'cod_bienes', FILTER_SANITIZE_STRING),
    ); //parametros de la llamada

    $client = new SoapClient($servicio, $parametros);

    $res = $client->getbien($parametros);//llamamos al método que nos interesa con los parámetros

    $resultado = $res->getbienResult->bienesData;
    $data = array();
    if (is_array($resultado)) {
        for ($i = 0; $i < count($resultado); $i++) {
            foreach ($resultado[$i] as $key => $value) {
                $data[$i][$key] = $value;

            }
        }

    } else {

        foreach ($resultado as $key => $value) {
            $data[$key] = $value;
            //echo $key;
            //die();
            if ($key == 'caracteristicas') {
                $arrCaracteristicas = (preg_split('~;~', substr($value, 0, -1)));
                $arrObjetosCaracteristica = [];
                foreach ($arrCaracteristicas as $arrObjeto) {
                    $dato = preg_split('~:~', $arrObjeto);
                    if (!empty($dato) and $dato[0] != " ") {
                        $arrObjetosCaracteristica[$dato[0]] = $dato[1];
                    }
                }
                $data[$key] = $arrObjetosCaracteristica;

            }
        }

    }
    if (count($data) > 0) {

        echo json_encode(array('status' => true, 'data' => $data));

    } else {

        echo json_encode(array('status' => false, 'data' => 'No hay datos'));

    }
}
?>
  