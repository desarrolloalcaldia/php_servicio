<?php

require_once 'conexion.php';


function listarEmpleadosContrato($nombre_completo)
{
    try {
        $conexion = new Conexion();
        $query = $conexion->dbh->prepare("SELECT 
          concat(ee.nombre,' ',ee.otro_nombre,' ',ee.paterno,' ',ee.materno) as nombre_completo,
          ee.otro_nombre,
          ee.nombre,
          ee.paterno,
          ee.materno,
          ec.nro_item,
          etp.descripcion as tipo_contrato,
          eca.descripcion                                     AS cargo,
          ea.descripcion                                      AS unidad,
          numdocumento,
          email,
          telefono,
          telefono_coorp,
          direccion
        FROM emp_empleado ee
          JOIN emp_contrato ec ON ec.id_empleado = ee.id_empleado and ec.actual=1::BIT
          JOIN emp_areatrabajo ea ON ec.id_area = ea.id_area
          JOIN emp_cargo eca ON ec.id_cargo = eca.id_cargo
          JOIN emp_tipocontrato etp ON ec.id_tipocontrato=etp.id_tipocontrato
        where concat(ee.nombre,' ',ee.paterno,' ',ee.materno) ILIKE '%" . $nombre_completo . "%'");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        $e->getMessage();
    }
}


header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=utf-8');


if (isset($_POST['nombre_completo'])) {
    $nombre_completo = $_POST['nombre_completo'];
    $empleados = listarEmpleadosContrato($nombre_completo);

    $data = array();
    if (is_array($empleados)) {
        foreach ($empleados as $key => $value) {
            $data[$key] = $value;
        }
    }

    if (count($data) > 0) {
        echo json_encode(array('status' => true, 'data' => $data));
    } else {
        echo json_encode(array('status' => false, 'data' => "No hay empleados"));
    }

} else {

    echo json_encode(array('status' => false, 'data' => 'No ingresaste datos'));

}
?>
  