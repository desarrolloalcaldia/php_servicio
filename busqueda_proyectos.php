<?php

        function objectToArray ($object) 
        {
            if(!is_object($object) && !is_array($object))
                return $object;
            return array_map('objectToArray', (array) $object);
        }

        $response = array();
        $response["result"] = array();
        $response["success"] = 0;
        $response["message"] = "";
       
        //$_POST['denominacion']= "alalay";
       

      if ( isset($_POST['denominacion']) )
      {
                
                $servicio="http://192.168.104.102/wsproyectos/Service.asmx?WSDL"; //url del servicio
                $parametros=array(); //parametros de la llamada
                $parametros['denominacion']=$_POST['denominacion'];

                $client = new SoapClient($servicio, $parametros);
                $resultado = $client->GetListaProyectos($parametros);//llamamos al métdo que nos interesa con los parámetros
                $en_array=objectToArray($resultado);

                /*
                echo("<pre>");
                print_r($en_array['GetListaProyectosResult']);
                echo("</pre>");
                echo("-------------->");
                exit(1);  
                */
                $data = array();
                if ((strlen($en_array['GetListaProyectosResult']['ListData']['estr_prog']))!=0)
                {

                          $base=$en_array['GetListaProyectosResult']['ListData'];
                          $part=array();
                          $part['ue']=$base['ue'];
                          $part['ue_desc']=$base['ue_desc'];
                          $part['estr_prog']=$base['estr_prog'];
                          $part['denominacion']=$base['denominacion'];
                          $part['monto_matriz']=number_format($base['monto_matriz'],2,',','.');

                          $part['fr_desc_etapa']=$base['fr_desc_etapa'];
                          $part['fr_eje_presup']=$base['fr_eje_presup'];
                          $part['fr_eje_fis']=$base['fr_eje_fis'];
                          //print_r($part);
                          array_push($data,$part);    
                }
                else
                {
                  foreach ($en_array as $key) 
                  {
                      
                      foreach ($en_array['GetListaProyectosResult']['ListData'] as $subelem) 
                      {
                          $part=array();
                          $part['ue']=$subelem['ue'];
                          $part['ue_desc']=$subelem['ue_desc'];
                          $part['estr_prog']=$subelem['estr_prog'];
                          $part['denominacion']=$subelem['denominacion'];
                          $part['monto_matriz']=number_format($subelem['monto_matriz'],2,',','.');
                          $part['fr_desc_etapa']=$subelem['fr_desc_etapa'];
                          $part['fr_eje_presup']=$subelem['fr_eje_presup'];
                          $part['fr_eje_fis']=$subelem['fr_eje_fis'];
                          array_push($data,$part);    
                      }
                  }

                }
                /*
                echo("<pre>");
                print_r($data);
                echo("</pre>");
                exit(1);
                */
                array_push($response['result'],$data);
                $response["success"] = 1;
                $response["message"] = "Listado entregado exitosamente";
                /*
                echo("<pre>");
                print_r($response);
                echo("</pre>");
                */
                echo(json_encode($response));
        }
        else
        {
            $response["success"] = 0;
            $response["message"] = "Campos requeridos no existentes";
            echo json_encode($response);
        }
?>
  