<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=utf-8');



$servicio = "http://192.168.104.102/wstramites/Service.asmx?WSDL"; //url del servicio


$client = new SoapClient($servicio);

$res = $client->getoficinas();//llamamos al método que nos interesa con los parámetros

$resultado = $res->getoficinasResult->OficinaData;

$data = array();
if (is_array($resultado)) {

    for ($i = 0; $i < count($resultado); $i++) {
        foreach ($resultado[$i] as $key => $value) {
            $data[$i][$key] = $value;
        }
    }

} else {

    foreach ($resultado as $key => $value) {
        $data[$key] = $value;
    }

}
if (count($data) > 0) {

    echo json_encode(array('status' => true, 'data' => $data));

} else {

    echo json_encode(array('status' => false, 'data' => 'No hay datos'));

}
?>
  