<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=utf-8');

if (isset($_POST['gestion']) and isset($_POST['number']) and is_numeric($_POST['gestion']) and is_numeric($_POST['number'])) {

    $servicio = "http://192.168.104.102/wstramites/Service.asmx?WSDL"; //url del servicio

    $parametros = array(
        'gestion' => filter_input(INPUT_POST, 'gestion', FILTER_SANITIZE_NUMBER_INT),
        'number' => filter_input(INPUT_POST, 'number', FILTER_SANITIZE_NUMBER_INT)
    ); //parametros de la llamada

    $client = new SoapClient($servicio, $parametros);

    $res = $client->GetTramiteTodo($parametros);//llamamos al métdo que nos interesa con los parámetros

    $resultado = $res->GetTramiteTodoResult->OffData;

    $data = array();
    if (is_array($resultado)) {

        for ($i = 0; $i < count($resultado); $i++) {
            foreach ($resultado[$i] as $key => $value) {
                $data[$i][$key] = $value;
            }
        }

    } else {

        foreach ($resultado as $key => $value) {
            $data[$key] = $value;
        }

    }
    if (count($data) > 0) {

        echo json_encode(array('status' => true, 'data' => $data));

    } else {

        echo json_encode(array('status' => false, 'data' => 'No hay datos'));

    }

} else {

    echo json_encode(array('status' => false, 'data' => 'No hay datos'));

}
?>
  