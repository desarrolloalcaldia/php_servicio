<?php

require_once 'conexion.php';


function listarEmpleadosContrato($numdocumento)
{
    try {
        $conexion = new Conexion();
        $query = $conexion->dbh->prepare("SELECT
  ee.id_empleado,
  cv.numdocumento,
  concat(COALESCE (ee.nombre,cv.nombre), ' ',COALESCE (ee.otro_nombre,cv.otro_nombre), ' ', COALESCE (ee.paterno,cv.paterno), ' ', COALESCE (ee.materno,cv.materno)) AS nombres,
  ec.nro_item,
  coalesce(eca.descripcion, cv.cargo)                 AS cargo,
  coalesce(ea.descripcion, cv.area)                   AS unidad,
  ee.fechanac,
  ee.sexo,
  CASE WHEN ee.acobro = 1 :: BIT
    THEN 'SI'
  ELSE 'NO' END                                       AS cobrado
FROM emp_empleado ee
  JOIN emp_contrato ec ON ee.id_empleado = ec.id_empleado
  JOIN emp_areatrabajo ea ON ec.id_area = ea.id_area and ec.actual = '1'
  JOIN emp_cargo eca ON ec.id_cargo = eca.id_cargo
  RIGHT JOIN cred_empleado cv ON cv.numdocumento = ee.numdocumento
WHERE cv.numdocumento = '" . $numdocumento . "' AND cv.activo = '1'");
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
        $conexion->dbh = null;
    } catch (PDOException $e) {
        $e->getMessage();
    }
}


header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=utf-8');


if (isset($_POST['numdocumento'])) {
    $numdocumento = $_POST['numdocumento'];
    $empleados = listarEmpleadosContrato($numdocumento);
    /*print_r($empleados);
    //print_r($numdocumento);
    die();*/
    $data = array();
    if (is_array($empleados)) {
        foreach ($empleados as $key => $value) {
            $data[$key] = $value;
        }
    }


    if (count($data) > 0) {

        echo json_encode(array('status' => true, 'data' => $data));

    } else {
        echo json_encode(array('status' => false, 'data' => "No hay empleados"));

    }

} else {

    echo json_encode(array('status' => false, 'data' => 'No ingresaste datos'));

}
?>
  